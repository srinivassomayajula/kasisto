@rem  Start Windows Explorer on a given directory

@setlocal

@set dir=%1

@set dir=%dir:/=\%

@start  ""  %SystemRoot%\Explorer.exe /n,/e,%dir%

