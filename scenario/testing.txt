TDX UI Tests


The test descriptions are in directory test\.
The individual tests are grouped in nested subdirectories.

  test\
  test\tmv\

There will ultimately be many tests, and more grouping subdirectories.

This directory tree structure should be maintained
when incorporating the tests into the Eclipse test framework.


Each individual test has its own scenario
embodied in a unique Java test program,
which collects its parameters from the corresponding spreadsheet.

Each Java test is programmed
from a textual description of the steps and its spreadsheet values.

The spreadsheet can be copied into the appropriate place in the Eclipse framework.


Each test is originally defined by several files with the same name
  NAME.note
    a textual description of the test steps
      to be processed in a Java test of the same NAME
    including how to interpret the spreadsheet columns
  NAME.xlxs
    a spreadsheet table of parameter values for the Java test program
      input
        plain strings
      output validation
        plain strings
        regular expressions
        file names for lists
          names of files (in directory list\) of lists of plain strings
    at least 2 rows
      top row is column names
        each is described in the corresponding NAME.note file
      other rows have data parameters for tests
  NAME.csv
    an optional table defining spreadsheet values
    it is not used to run a test
    but it may be initially helpful when first writing a test



