Instructions for QAMentor for TDX UI testing ...


The implementation process for each test will involve
  evaluating a description of test steps and it spreadsheet of parameters
  creating a Java test program
  copying the spreadsheet into the appropriate place in the Eclipse framework

There is a small starter set of tests already definedfor implementation.
Eventually there will be many more tests grouped into subdirectories.
Occasionally, exisitng tests and/or their parameters may get expanded.

Previously implemented demo tests are not part of this structure,
and should be moved into a separate demo\ subdirectory.


There is a new directory on the test system af302.appsforte.com
  c:\scenario\
with source test descriptions and spreadsheets
which will be used to implement tests.
More items will be added as they become available.

The new material contains

  c:\scenario\testing\
    lists of tests to implement
      tests.2019.07.02.list
        a few tests to implement first
    each item names a pair of files defining a test to implement
      NAME.note
      NAME.xlsx
    more lists and tests will be added later

  c:\scenario\testing.txt
    some general details about
      the test steps
      the test parameters
      the implementation process

  c:\scenario\test\
    a growing directory of all the tests grouped in subdirectories
      initially there are only a few tests in only 1 subdirectory
      eventually there will be many more tests and more subdirectories
    each test is define by 2 files
      a textual description of the steps
      a spreadsheet of parameter values

  c:\scenario\list\
    some plain text files of lists of strings used by some tests


Please inform this Slack thread about questions, problems, and progress
about implementing the first tests named in tests.2019.07.02.list.

  tmv\tmv.filtering         Verify proper available choices in most TMV dropdowns
  tmv\tmv.JobTitle          Do a simple TMV analyses for a job title
  tmv\tmv.JobTitle.compare  Do 2 simple TMV analyses for job titles and compare them
  tmv\tmv.RateUnit          Do an extended TMV analyses for a job title with a rate unit


