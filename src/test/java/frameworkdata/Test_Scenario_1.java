/**
 * 
 */
package frameworkdata;

import org.testng.asserts.SoftAssert;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;


import testbase.base;
import uiActions.util;




public class Test_Scenario_1 extends base{
	//public static WebDriver driver;
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(Test_Scenario_1.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	
	@BeforeTest()
	public void driverinitialize() throws IOException{
		test = rep.startTest("Test_Scenario_1");
		test.log(LogStatus.INFO, "Starting the Test_Scenario_1 for Kasisto");
		initializeDriver();		
	}

	
	@Test(priority=1)
	public void Login() throws InterruptedException 
	{
		driver.navigate().to(prop.getProperty("url1"));
		test.log(LogStatus.PASS, "Opening URL " + prop.getProperty("url1") );
		
		u.type("kasisto_username_xpath", prop.getProperty("username1"));
		test.log(LogStatus.PASS, "Enter UserName :  " + prop.getProperty("username1"));
		
		u.type("kasisto_password_xpath", prop.getProperty("password1"));
		test.log(LogStatus.PASS, "Enter PassWord :  " + prop.getProperty("password1"));
		
		u.click("kasisto_login_xpath");
		test.log(LogStatus.PASS, "Click 'Login' Button" );
		
		u.waitForElementClickable(driver, getElement("Annotations_xpath"));
		u.click("Annotations_xpath");
		test.log(LogStatus.PASS, "Clicking  Annotations link" );
		
		u.waitForElementVisibility(driver, getElement("Reports_xpath"));
		u.click("Reports_xpath");
		test.log(LogStatus.PASS, "Clicking  reports link" );
		
		String IntentReportLink = getElement("Intent_report_xpath").getText();
		test.log(LogStatus.PASS, "Verifying " + IntentReportLink + " is present " );
		
		
		//u.waitForElement(driver, 10, getElement("Intent_report_xpath"));
		u.waitForElementVisibility(driver, getElement("Intent_report_xpath"));
		u.click("Intent_report_xpath");
		test.log(LogStatus.PASS, "Clicking Intent reports" );
		
		u.isElementDisplayed(getElement("Intent_report_page_xpath"));
		test.log(LogStatus.PASS, "Verifying Intent Report  is displayed " );
		
		u.click("Today_tab_xpath");
		test.log(LogStatus.PASS, "Clicking today tab" );
		
		String FirstIntentName = getElement("First_intent_name_xpath").getText();
		test.log(LogStatus.PASS, "Retriving First intent name ie. [" + FirstIntentName + "]" );	
		String TotalAnnotations = getElement("Total_annotations_xpath").getText();
		String TotalCompleted = getElement("Total_completed_xpath").getText();
		String CorrectedFrom = getElement("Corrected_from_xpath").getText();
		String CorrectedTo = getElement("Corrected_to_xpath").getText();
		String FirstAnnotaitons = getElement("1st_intent_annotations_column_xpath").getText();
		String FirstTotalCompleted = getElement("1st_intent_total_completed_column_xpath").getText();
		String FirstCorrectedFrom = getElement("1st_intent_corrected_from_column_xpath").getText();
		String FirstCorrectedto = getElement("1st_intent_corrected_to_column_xpath").getText();
		test.log(LogStatus.PASS, "First intent name and total annotations data are [" + FirstIntentName + "] are [" + TotalAnnotations + "]" + "[" + FirstAnnotaitons + "]");
		test.log(LogStatus.PASS, "First intent name and total completed data are [" + FirstIntentName + "] are [" + TotalCompleted + "]" + "[" + FirstTotalCompleted  + "]");
		test.log(LogStatus.PASS, "First intent name and corrected from data are [" + FirstIntentName + "] are [" + CorrectedFrom + "]" + "[" + FirstCorrectedFrom  + "]");
		test.log(LogStatus.PASS, "First intent name and corrected to data are [" + FirstIntentName + "] are [" + CorrectedTo + "]" + "[" + FirstCorrectedto  + "]");
		
		u.click("Annotations_overview_xpath");
		test.log(LogStatus.PASS, "Clicking annotations overview link" );
		
		u.isElementDisplayed(getElement("Annotation_sets_xpath"));
		test.log(LogStatus.PASS, "Verifying Annotation sets are displayed");
		u.takeScreenShot();
		
		u.click("For_manager_approval_xpath");
		test.log(LogStatus.PASS, "Clicking For manager approval button" );
		
		u.isElementDisplayed(getElement("For_manager_approval_xpath"));
		test.log(LogStatus.PASS, "Verifying For manager approval page displayed");
		u.takeScreenShot();
		
		u.click("Fix_conflicts_xpath");
		test.log(LogStatus.PASS, "Clicking Fix conflicts button" );
		
		u.isElementDisplayed(getElement("Fix_conflicts_page_xpath"));
		test.log(LogStatus.PASS, "Verifying Fix conflict page displayed");
		u.takeScreenShot();
		
		u.waitForElementClickable(driver, getElement("Intent_selectbox_xpath"));
		u.click("Intent_selectbox_xpath");
		test.log(LogStatus.PASS, "Clicking Intent select box" );
		u.waitToLoad();
		
		u.type("Intent_input_search_xpath", FirstIntentName);
		u.click("Intent_search_select_xpath");
		
		test.log(LogStatus.PASS, "Clicking Intent search to enter 1st collected intent" );
		u.click("Copy_this_answer_xpath");
		
		test.log(LogStatus.PASS, "Clicking Copy this answer button" );
		u.isElementDisplayed(getElement("Annotation_conflict_resolved_xpath"));
		u.takeScreenShot();
		
		u.click("Reports_xpath");
		test.log(LogStatus.PASS, "Clicking  reports link" );
		
		u.click("Intent_report_xpath");
		test.log(LogStatus.PASS, "Clicking Intent reports" );
		
		u.click("Today_tab_xpath");
		test.log(LogStatus.PASS, "Clicking today tab" );
		
		u.waitForElementVisibility(driver, getElement("Total_annotations_xpath"));
		String TotalAnnotations1 = getElement("Total_annotations_xpath").getText();
		String TotalCompleted1 = getElement("Total_completed_xpath").getText();
		String CorrectedFrom1 = getElement("Corrected_from_xpath").getText();
		String CorrectedTo1 = getElement("Corrected_to_xpath").getText();
		String FirstAnnotaitons1 = getElement("1st_intent_annotations_column_xpath").getText();
		String FirstTotalCompleted1 = getElement("1st_intent_total_completed_column_xpath").getText();
		String FirstCorrectedFrom1 = getElement("1st_intent_corrected_from_column_xpath").getText();
		String FirstCorrectedto1 = getElement("1st_intent_corrected_to_column_xpath").getText();
		int TA1=Integer.parseInt(FirstAnnotaitons1);
		int TA=Integer.parseInt(FirstAnnotaitons);
		if (TA1>=TA) {
			test.log(LogStatus.PASS, "Verifying First intents Total Annotations values after fixing conflicts is hihger(increased) or equal. [" + FirstIntentName + "] are [" + TotalAnnotations + "]" + "[" + FirstAnnotaitons  +"] = ["+ TotalAnnotations1 + "]" + "[" + FirstAnnotaitons1 +"] ");
		}
		else {
			test.log(LogStatus.FAIL, "Verifying First intents Total Annotations values after fixing conflicts is lower or decreased. [" + FirstIntentName + "] are [" + TotalAnnotations + "]" + "[" + FirstAnnotaitons  +"] = ["+ TotalAnnotations1 + "]" + "[" + FirstAnnotaitons1 +"] ");
		}
		int TC1=Integer.parseInt(FirstTotalCompleted1);
		int TC=Integer.parseInt(FirstTotalCompleted);
		if (TC1>=TC) {
			test.log(LogStatus.PASS, "Verifying First intent names Total Completed values after fixing conflicts is hihger(increased) or equal. [" + FirstIntentName + "] are [" + TotalCompleted + "]" + "[" + FirstTotalCompleted +"] = ["+ TotalCompleted1 + "]" + "[" + FirstTotalCompleted1 +"] ");
		}
		else {
			test.log(LogStatus.FAIL, "Verifying First intent names Total Completed values after fixing conflicts is lower or decreased. [" + FirstIntentName + "] are [" + TotalCompleted + "]" + "[" + FirstTotalCompleted +"] = ["+ TotalCompleted1 + "]" + "[" + FirstTotalCompleted1 +"] ");
		}
		int CF1=Integer.parseInt(FirstCorrectedFrom1);
		int CF=Integer.parseInt(FirstCorrectedFrom);
		if (CF1>=CF) {
			test.log(LogStatus.PASS, "Verifying First intent names Corrected From values after fixing conflicts is hihger(increased) or equal. [" + FirstIntentName + "] are [" + CorrectedFrom + "]" + "[" + FirstCorrectedFrom  +"] = ["+ CorrectedFrom1 + "]" + "[" + FirstCorrectedFrom1 +"] ");
		} 
		else {
			test.log(LogStatus.FAIL, "Verifying First intent name and  Corrected From values after fixing conflicts is lower or decreased. [" + FirstIntentName + "] are [" + CorrectedFrom + "]" + "[" + FirstCorrectedFrom  +"] = ["+ CorrectedFrom1 + "]" + "[" + FirstCorrectedFrom1 +"] ");
		}
		int CT1=Integer.parseInt(FirstCorrectedto1);
		int CT=Integer.parseInt(FirstCorrectedto);
		if (CT1>=CT) {
			test.log(LogStatus.PASS, "Verifiying First intent name and  Collected To values after fixing conflicts is hihger(increased) or equal. [" + FirstIntentName + "] are [" + CorrectedTo + "]" + "[" + FirstCorrectedto  +"] = ["+ CorrectedTo1 + "]" + "[" + FirstCorrectedto1 +"] ");
		} 
		else {
			test.log(LogStatus.FAIL, "Verifying First intent name and  Collected To values after fixing conflicts is lower or decreased. [" + FirstIntentName + "] are [" + CorrectedTo + "]" + "[" + FirstCorrectedto  +"] = ["+ CorrectedTo1 + "]" + "[" + FirstCorrectedto1 +"] ");
		}
		u.takeScreenShot();
		u.click("User_account_tab_xpath");
		test.log(LogStatus.PASS, "Clicking User account tab for logging out" );
		
		u.click("logout_xpath");
		test.log(LogStatus.PASS, "Clicking logout" );
		
		
		
	}
		
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}
	
	
	
	
	
}

