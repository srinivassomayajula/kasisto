/**
 * 
 */
package frameworkdata;

import org.testng.asserts.SoftAssert;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hamcrest.core.Is;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import listners.listners;
import testbase.base;
import uiActions.util;




public class Test_Scenario_3 extends base{
	//public static WebDriver driver;
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(Test_Scenario_3.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	
	@BeforeTest()
	public void driverinitialize() throws IOException{
		test = rep.startTest("Test_Scenario_3");
		test.log(LogStatus.INFO, "Starting the Test_Scenario_3 for Kasisto");
		initializeDriver();		
	}

	
	@Test(priority=1)
	public void Login() throws InterruptedException 
	{
		driver.navigate().to(prop.getProperty("url1"));
		test.log(LogStatus.PASS, "Opening URL " + prop.getProperty("url1") );
		
		u.type("kasisto_username_xpath", prop.getProperty("username1"));
		test.log(LogStatus.PASS, "Enter UserName :  " + prop.getProperty("username1"));
		
		u.type("kasisto_password_xpath", prop.getProperty("password1"));
		test.log(LogStatus.PASS, "Enter PassWord :  " + prop.getProperty("password1"));
		
		u.click("kasisto_login_xpath");
		test.log(LogStatus.PASS, "Click 'Login' Button" );
		
		u.click("Analytics_xpath");
		test.log(LogStatus.PASS, "Clicking Analytics link" );
		
		u.isElementDisplayed(getElement("Dashboard_xpath"));
		u.click("Dashboard_xpath");
		test.log(LogStatus.PASS, "Clicking dashboard link" );
		
		u.isElementDisplayed(getElement("Dashboard_page_xpath"));
		test.log(LogStatus.PASS, "Verifying Dashboard psge is displayed" );
		
		String ReqCount = getElement("Requests_count_xpath").getText().replaceAll("[,^]*", "").toString();
		test.log(LogStatus.PASS, "Retriveing requests count i.e.[" + ReqCount + "]");
		
		u.click("Devices_link_xpath");
		test.log(LogStatus.PASS, "Clicking the devices link" );
		
		List<WebElement> elements = driver.findElements(By.xpath("//th[contains(.,'Requests')]/ancestor::thead/following-sibling::tbody/descendant::td"));
	    //System.out.println("Number of elements:" + elements.size());
		u.takeScreenShot();
		
	    int sum = 0 ;
	    for (int i=3; i<elements.size();i=i+4){
	    String ReqDev = elements.get(i).getText().toString();
	    //System.out.println(ReqDev);
	    test.log(LogStatus.PASS, "geting values from devices page [" +elements.get(i).getText()+ "]");
	    
	    int J = Integer.parseInt(ReqDev);
		//System.out.println(J);
	    sum = sum + J;
	    }
		System.out.println(sum);
		test.log(LogStatus.PASS, "  " + sum );
		
		
		int RC = Integer.parseInt(ReqCount);
		if (RC==sum) {
			test.log(LogStatus.PASS, " Verifying Sum the total number of Requests present in Devices table is [" +RC+ "] is equal to Sum the total number of Requests present in Devices table  [" + sum + "]");
		}
		else {
			test.log(LogStatus.FAIL, " Verifying Sum the total number of Requests present in Devices table is [" +RC+ "] is not equal to Sum the total number of Requests present in Devices table  [" + sum + "]");
		}
		
		u.click("User_account_tab_xpath");
		test.log(LogStatus.PASS, "Clicking User account tab for logging out" );
		
		u.click("logout_xpath");
		test.log(LogStatus.PASS, "Clicking logout" );
		
		
		
		
	}
	
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}
}