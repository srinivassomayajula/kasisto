/**
 * 
 */
package frameworkdata;

import org.testng.asserts.SoftAssert;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hamcrest.core.Is;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;


import testbase.base;
import uiActions.util;




public class Test_Scenario_2 extends base{
	//public static WebDriver driver;
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(Test_Scenario_2.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	
	@BeforeTest()
	public void driverinitialize() throws IOException{
		test = rep.startTest("Test_Scenario_2");
		test.log(LogStatus.INFO, "Starting the Test_Scenario_2 for Kasisto");
		initializeDriver();		
	}

	
	@Test(priority=1)
	public void Login() throws InterruptedException 
	{
		driver.navigate().to(prop.getProperty("url1"));
		test.log(LogStatus.PASS, "Opening URL " + prop.getProperty("url1") );
		
		u.type("kasisto_username_xpath", prop.getProperty("username1"));
		test.log(LogStatus.PASS, "Enter UserName :  " + prop.getProperty("username1"));
		
		u.type("kasisto_password_xpath", prop.getProperty("password1"));
		test.log(LogStatus.PASS, "Enter PassWord :  " + prop.getProperty("password1"));
		
		u.click("kasisto_login_xpath");
		test.log(LogStatus.PASS, "Click 'Login' Button" );
		
		u.click("Conversation_management_xpath");
		test.log(LogStatus.PASS, "Click Converesation Management tab" );
		
		u.click("FAQ_xpath");
		test.log(LogStatus.PASS, "Click FAQ tab" );
		
		u.waitForElementVisibility(driver, getElement("FAQ_page_xpath"));
		u.isElementDisplayed(getElement("FAQ_page_xpath"));
		test.log(LogStatus.PASS, "Verifying FAQ page is displayed" );
		u.takeScreenShot();
		
		u.click("FAQ_random_question_xpath");
		test.log(LogStatus.PASS, "selecting first available question" );
		u.waitToLoad();
		
		//u.waitForElement(driver, 10, getElement("FAQ_default_edit_xpath"));
		//u.waitForElementClickable(driver, getElement("FAQ_default_edit_xpath"));
		WebElement element = getElement("FAQ_default_edit_xpath");
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();
		test.log(LogStatus.PASS, "Clicking first FAQ edit button" );
		
		//u.click("FAQ_default_edit_xpath");
		u.click("Button_templete_xpath");
		test.log(LogStatus.PASS, "Click Button templete tab" );
		
		u.clear("Text_response_area_xpath");
		test.log(LogStatus.PASS, "Clearing the text in text box area" );
		
		u.type("Text_response_area_xpath", prop.getProperty("Enter_text"));
		test.log(LogStatus.PASS, "Entering some random text in Text response area" );
		
		u.clear("Enter_title_xpath");
		test.log(LogStatus.PASS, "Clearing the title text box" );
		
		u.type("Enter_title_xpath", prop.getProperty("Enter_title"));
		test.log(LogStatus.PASS, "Entering some title from properties ie. [" + prop.getProperty("Enter_title") + "]" );
		
		u.clear("Enter_URL_xpath");
		test.log(LogStatus.PASS, "Clearing URL text box" );
		
		u.type("Enter_URL_xpath", prop.getProperty("url1"));
		test.log(LogStatus.PASS, "Entering Url from properties i.e [" + prop.getProperty("url1")+ "]" );
		
		u.click("Save_answer_button_xpath");
		test.log(LogStatus.PASS, "Clicking save button to save given data" );
		
		String SavedMessage1 = getElement("Saved_sucessfully_xpath").getText();
		test.log(LogStatus.PASS, "Click 'Login' Button [" + SavedMessage1 + "]"  );
		
		u.isElementDisplayed(getElement("Saved_sucessfully_xpath"));
		test.log(LogStatus.PASS, "Verifying successfully saved message" );
		u.takeScreenShot();
		
		u.click("Ok_button_xpath");
		test.log(LogStatus.PASS, "Clicking ok button" );
		
		u.scrollTo(driver, getElement("Question_heading_xpath"));
		WebElement element1 = getElement("FAQ_default_edit_xpath");
		Actions actions1 = new Actions(driver);
		actions1.moveToElement(element1).click().build().perform();
		test.log(LogStatus.PASS, "Clicking FAQ edit button to enter data again." );
		
		//u.click("FAQ_default_edit_xpath");
		u.clear("Text_response_area_xpath");
		test.log(LogStatus.PASS, "Clearing the text in text box area" );
		
		u.type("Text_response_area_xpath", prop.getProperty("Enter_title"));
		test.log(LogStatus.PASS, "Entering some random text in Text response area" );
		
		u.click("Alternate_text_response_xpath");
		test.log(LogStatus.PASS, "Clearing alternate text response area" );
		
		u.clear("Alternate_enter_text_xpath");
		test.log(LogStatus.PASS, "Clearing the text in alternate text response area" );
		
		u.type("Alternate_enter_text_xpath", prop.getProperty("Enter_text1"));
		test.log(LogStatus.PASS, "Entering random text in alternate text response area" );
		
		u.click("Save_answer_button_xpath");
		test.log(LogStatus.PASS, "Clicking save answer button to save the given data" );
		
		String SavedMessage2 = getElement("Saved_sucessfully_xpath").getText();
		test.log(LogStatus.PASS, "Click 'Login' Button [" + SavedMessage2 + "]"  );
		
		u.isElementDisplayed(getElement("Saved_sucessfully_xpath"));
		test.log(LogStatus.PASS, "Verifying saved successfully message is displayed" );
		u.takeScreenShot();
		
		u.click("Ok_button_xpath");
		test.log(LogStatus.PASS, "Clicking ok button" );
		
		String verifyTextResonse1 = getElement("Text_response_area_xpath").getText();
		u.verifyTextEquals(getElement("Text_response_area_xpath"), verifyTextResonse1);
		test.log(LogStatus.PASS, "Verifiying text response area i.e. [" + verifyTextResonse1 + "] is equals to [" + prop.getProperty("Enter_text") + "]" );
		
		String verifiyTextResponse2 = getElement("Alternate_enter_text_xpath").getText();
		u.verifyTextEquals(getElement("Alternate_enter_text_xpath"), verifiyTextResponse2);
		test.log(LogStatus.PASS, "Verifying alternate text response area i.e. [" + verifiyTextResponse2 + "] is equals to [" + prop.getProperty("Enter_text1") + "]" );
		u.scrollTo(driver, getElement("Question_heading_xpath"));
		u.takeScreenShot();
		
		u.click("User_account_tab_xpath");
		test.log(LogStatus.PASS, "Clicking User account tab for logging out" );
		
		u.click("logout_xpath");
		test.log(LogStatus.PASS, "Clicking logout" );
		
		
		
		
		
		
		
	}
	
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}
	
}